import sys, getopt, requests, itertools, threading, time
from bs4 import BeautifulSoup

done = False
#here is the animation
def animate():
    for c in itertools.cycle(['|', '/', '-', '\\']):
        if done:
            break
        sys.stdout.write('\rloading ' + c)
        sys.stdout.flush()
        time.sleep(0.1)
    sys.stdout.write('\rDone!     ')

#fetch german word from frag caeser
def fetch_word(url):
    url2 = "".join(url)
    page = requests.get(url2)
    soup = BeautifulSoup(page.content, "html.parser")
    soup2 = soup.find("div", class_="table-responsive")
    if soup2 != None:
        word = soup2.find_all("td", class_="eh")[-1].get_text(separator=";")
        word = word.split(";")[0]
    else:
        word = []
        for i in range(len(soup.find_all("li", class_="list-group-item list-toggle"))):
            url2 = url
            url2[3:5] = ["-",i+1]
            url2 = "".join((str(i) for i in url2))
            page = requests.get(url2)
            soup = BeautifulSoup(page.content, "html.parser")
            soup2 = soup.find("div", class_="table-responsive")
            word2 = soup2.find_all("td", class_="eh")[-1].get_text(separator=";")
            word2 = word2.split(";")[0]
            if word2 == "": word2 = "/"
            word.append(word2)
        word = "|".join(word)
    return word

def get_word(latin_word):
    de_word = ""
    latin_word = ''.join(e for e in latin_word.lower() if e.isalnum())
    latin_word = "".join(latin_word.split()) 
    URL = ["https://www.frag-caesar.de/lateinwoerterbuch/", latin_word, "-uebersetzung","", "",".html"]
    de_word = fetch_word(URL)
    return "(" + de_word + ")"
 

def latin_from_line(latin_line):
    la_words = latin_line.split()
    de_words = []
    for la_word in la_words:
        
        de_words.append(get_word(la_word))
    return " ".join(de_words)

def main(argv):
    inputfile = ''
    outputfile = ''
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print(sys.argv[0] + ' -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print(sys.argv[0] + ' -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
    latin_txt_open = open(inputfile, "r")
    f = open(outputfile, "w")

    t = threading.Thread(target=animate) # start animation
    t.daemon = True
    t.start()
    for line in latin_txt_open:
        f.write(line)
        f.write(latin_from_line(line))
        f.write("\n\n")
    f.close()
    done = True 
    #end animation

if __name__ == "__main__":
   main(sys.argv[1:])

